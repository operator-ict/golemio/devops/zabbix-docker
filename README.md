## What is Zabbix?

[Zabbix](https://www.zabbix.com/) is an enterprise-class open source distributed monitoring solution.

## Docker compose

This docket compose include:
* PostgreSQL
* Zabbix-server
* Zabbix-agent
* Nginx
* Traefik
as OS is used Apline Linux.

[Original source](https://github.com/zabbix/zabbix-docker)

## How to run
```bash
docker network create traefik
git clone git@gitlab.com:operator-ict/golemio/devops/zabbix-docker.git
cd zabbix-docker
mkdir secret
git clone git@gitlab.com:operator-ict/security/hub.git secret
docker-compose -f traefik up -d
docker-compose -f docker-compose_v3_alpine_pgsql.yaml up -d
```
